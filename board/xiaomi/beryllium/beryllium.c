// SPDX-License-Identifier: GPL-2.0+
/*
 * This empty file prevents make linking error.
 * No custom logic for beryllium so far.
 *
 * (C) Copyright 2022 Joel Selvaraj <jo@jsfamily.in>
 *
 */

void nooop(void) {}
